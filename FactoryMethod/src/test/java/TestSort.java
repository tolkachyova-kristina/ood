import org.junit.Test;

public class TestSort {
    @Test
    public void testSort1() {
        int[] array = new int[15];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 100) - 50);
        }
        IntArraySortingMachine bubbleAlg = new IntArraySortingMachine(new BubbleSort());
        bubbleAlg.sortArray(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

    }

    @Test
    public void testSort2() {
        int[] array = new int[15];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 100) - 50);
        }
        IntArraySortingMachine insertionAlg = new IntArraySortingMachine(new InsertionSort());
        insertionAlg.sortArray(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    @Test
    public void testSort3() {
        int[] array = new int[15];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 100) - 50);
        }
        IntArraySortingMachine selectionAlg = new IntArraySortingMachine(new SelectionSort());
        selectionAlg.sortArray(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
}
