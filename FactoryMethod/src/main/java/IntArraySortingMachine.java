public class IntArraySortingMachine {
    private ISort sortingAlgorithm;

    public IntArraySortingMachine(ISort sortingAlgorithm) {
        this.sortingAlgorithm = sortingAlgorithm;
    }
    public void sortArray(int[] array){
        sortingAlgorithm.sort(array);
    }
}
