package devaice.model.prototipes;


import devaice.model.decorator.HighlightingDevice;
import devaice.model.device.BluetoothHeadphone;
import devaice.model.device.USBMouse;
import devaice.model.factories.BluetoothDeviceFactory;
import devaice.model.factories.USBDeviceFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
public class DeviceTest {
    @Test
    public void testDevice() {
        DevicePrototypes.getInstance();
        BluetoothDeviceFactory bf = new BluetoothDeviceFactory();
        USBDeviceFactory uf = new USBDeviceFactory();
        BluetoothHeadphone headphone = bf.createHeadphone();
        USBMouse mouse = uf.createMouse();
        assertEquals(headphone.toString(),"Bluetooth Headphone : company name ='bron', description =''");
        assertEquals(mouse.toString(),"USB Mouse : company name ='logitec pro', description ='best'");
        HighlightingDevice highlightingHeadphone = new HighlightingDevice(headphone,"red");
        assertEquals(highlightingHeadphone.toString(),"Bluetooth Headphone : company name ='bron', description ='' and shine red color.");
    }
}
