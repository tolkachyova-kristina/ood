package devaice.interafce;

public interface Headphone {
    void turnUp();
    void turnDown();
}
