package devaice.interafce;

public interface IFactory {
    Headphone createHeadphone();
    Keyboard createKeyboard();
    Mouse createMouse();
}
