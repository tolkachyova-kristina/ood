package devaice.model.factories;

import devaice.interafce.IFactory;
import devaice.interafce.Headphone;
import devaice.interafce.Keyboard;
import devaice.interafce.Mouse;
import devaice.model.device.BluetoothHeadphone;
import devaice.model.device.BluetoothKeyboard;
import devaice.model.device.BluetoothMouse;
import devaice.model.prototipes.DevicePrototypes;

public class BluetoothDeviceFactory implements IFactory {

    public BluetoothHeadphone createHeadphone() {
        return (BluetoothHeadphone) DevicePrototypes.getDevicePrototype("BluetoothHeadphone");
    }

    public BluetoothKeyboard createKeyboard() {
        return (BluetoothKeyboard) DevicePrototypes.getDevicePrototype("BluetoothKeyboard");
    }

    public BluetoothMouse createMouse() {
        return (BluetoothMouse) DevicePrototypes.getDevicePrototype("BluetoothMouse");
    }
}
