package devaice.model.factories;

import devaice.interafce.IFactory;
import devaice.interafce.Headphone;
import devaice.interafce.Keyboard;
import devaice.interafce.Mouse;
import devaice.model.device.USBHeadphone;
import devaice.model.device.USBKeyboard;
import devaice.model.device.USBMouse;
import devaice.model.prototipes.DevicePrototypes;

public class USBDeviceFactory implements IFactory {

    public USBHeadphone createHeadphone() {
        return (USBHeadphone) DevicePrototypes.getDevicePrototype("USBHeadphone");
    }

    public USBKeyboard createKeyboard() {
        return (USBKeyboard) DevicePrototypes.getDevicePrototype("USBKeyboard");
    }

    public USBMouse createMouse() {
        return (USBMouse) DevicePrototypes.getDevicePrototype("USBMouse");
    }
}
