package devaice.model.device;

import devaice.interafce.Keyboard;

public class BluetoothKeyboard extends Device implements Keyboard {
    public BluetoothKeyboard(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new BluetoothKeyboard(getCompany(),getDescription());
    }

    public void pressButton() {

    }

    @Override
    public String toString() {
        return "Bluetooth Keyboard : " + super.toString();
    }
}
