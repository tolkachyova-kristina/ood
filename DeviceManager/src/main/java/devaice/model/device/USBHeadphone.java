package devaice.model.device;

import devaice.interafce.Headphone;

public class USBHeadphone extends Device implements Headphone {

    public USBHeadphone(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new USBHeadphone(getCompany(),getDescription());
    }

    public void turnUp() {

    }

    public void turnDown() {

    }

    @Override
    public String toString() {
        return "USB Headphone : " + super.toString();
    }
}
