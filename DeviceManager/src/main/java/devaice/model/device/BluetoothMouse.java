package devaice.model.device;

import devaice.interafce.Mouse;

public class BluetoothMouse extends Device implements Mouse {
    public BluetoothMouse(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new BluetoothMouse(getCompany(),getDescription());
    }

    public void rightClick() {

    }

    public void leftClick() {

    }

    @Override
    public String toString() {
        return "Bluetooth Mouse : " + super.toString();
    }
}
