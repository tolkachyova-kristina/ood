package devaice.model.device;

import devaice.interafce.Headphone;

public class BluetoothHeadphone extends Device implements Headphone {

    public BluetoothHeadphone(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new BluetoothHeadphone(getCompany(),getDescription());
    }

    public void turnUp() {
    }

    public void turnDown() {
    }

    @Override
    public String toString() {
        return "Bluetooth Headphone : " + super.toString();
    }
}
