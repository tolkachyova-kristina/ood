package devaice.model.device;

import devaice.interafce.Mouse;

public class USBMouse extends Device implements Mouse {
    public USBMouse(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new USBMouse(getCompany(),getDescription());
    }

    public void rightClick() {

    }

    public void leftClick() {

    }

    @Override
    public String toString() {
        return "USB Mouse : " + super.toString();
    }
}
