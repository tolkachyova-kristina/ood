package devaice.model.device;

import devaice.model.IDevice;

public abstract class Device implements IDevice {
    private String company;
    private String description;

    public Device(String company, String description) {
        this.company = company;
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public abstract Device getClone();

    @Override
    public String toString() {
        return "company name ='" + company + '\'' +
                ", description ='" + description + '\'';
    }
}
