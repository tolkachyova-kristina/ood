package devaice.model.device;

import devaice.interafce.Keyboard;

public class USBKeyboard extends Device implements Keyboard {

    public USBKeyboard(String company, String description) {
        super(company, description);
    }

    public Device getClone() {
        return new USBKeyboard(getCompany(),getDescription());
    }

    public void pressButton() {

    }

    @Override
    public String toString() {
        return "USB Keyboard : " + super.toString();
    }
}
