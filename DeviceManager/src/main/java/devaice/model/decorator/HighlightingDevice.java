package devaice.model.decorator;

import devaice.model.device.Device;

public class HighlightingDevice extends DecorateDevice{
    private String color;


    public HighlightingDevice(Device wrapped, String color) {
        super(wrapped);
        this.color = color;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(" and shine " + color + " color.");
    }
}
