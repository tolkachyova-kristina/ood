package devaice.model.decorator;

import devaice.model.IDevice;
import devaice.model.device.Device;

public class DecorateDevice implements IDevice {
    private Device wrapped;

    public DecorateDevice (Device wrapped) {
        this.wrapped = wrapped;
    }

    public String getCompany() {
        return wrapped.getCompany();
    }

    public void setCompany(String company) {
        wrapped.setCompany(company);
    }

    public String getDescription() {
        return wrapped.getDescription();
    }

    public void setDescription(String description) {
        wrapped.setCompany(description);
    }

    public Device getClone() {
        return wrapped.getClone();
    }
    public String toString(){
        return wrapped.toString();
    }
}
