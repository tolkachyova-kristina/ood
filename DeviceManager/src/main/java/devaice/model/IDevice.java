package devaice.model;

import devaice.model.device.Device;

public interface IDevice {
    String getCompany();
    void setCompany(String company);
    String getDescription();
    void setDescription(String description) ;
    abstract Device getClone();
    String toString();
}
