package devaice.model.prototipes;

import devaice.model.device.*;

import java.util.Hashtable;

public class DevicePrototypes {
    private static DevicePrototypes instance;
    private static Hashtable<String, Device> map;
    private DevicePrototypes(){
        map = new Hashtable<String, Device>();

        map.put("BluetoothHeadphone",new BluetoothHeadphone("bron",""));
        map.put("BluetoothMouse",new BluetoothMouse("smartbuy",""));
        map.put("BluetoothKeyboard",new BluetoothKeyboard("dexp","pice of trash"));
        map.put("USBMouse", new USBMouse("logitec pro","best"));
        map.put("USBKeyboard",new USBKeyboard("HyperX", ""));
        map.put("USBHeadphone", new USBHeadphone("myth", "legendary item"));
    }
    public static DevicePrototypes getInstance() {
        if (instance == null) {
            instance = new DevicePrototypes();
        }
        return instance;
    }
    public static Device getDevicePrototype(String type){

        return map.get(type);
    }
}
