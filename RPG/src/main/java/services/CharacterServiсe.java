package services;

import builders.models.EnemyBuilder;
import builders.models.HeroBuilder;
import builders.models.СharacterMaker;
import characters.components.CharacterClass;
import characters.interfaces.ICharacter;
import characters.model.Hero;

import java.util.HashMap;
import java.util.Map;

public class CharacterServiсe {
    private static CharacterServiсe characterServiсe;
    private static Map<String, HeroSnapShot> saves;
    public static CharacterServiсe getInstance() {
        if (characterServiсe == null) {
            characterServiсe = new CharacterServiсe();
            saves = new HashMap<String, HeroSnapShot>();
        }
        return characterServiсe;
    }
    public static ICharacter madeCharacter(CharacterClass characterClass){
        СharacterMaker maker = new СharacterMaker();
        HeroBuilder hb = new HeroBuilder();
        EnemyBuilder eb = new EnemyBuilder();
        ICharacter res;
        if(characterClass == CharacterClass.BOW){
            maker.createBowHero(hb);
            res = hb.getResult();
            return res;
        }else if(characterClass == CharacterClass.CATALYST){
            maker.createCatalystHero(hb);
            res = hb.getResult();
            return res;
        }else if(characterClass == CharacterClass.SWORD){
            maker.createSwordHero(hb);
            res = hb.getResult();
            return res;
        }else if(characterClass == CharacterClass.MONSTER){
            maker.createEnemy(eb);
            res = eb.getResult();
            return res;
        }
        return null;
    }
    public static void saveHeroStatus(Hero ch){
        if(saves.containsKey(ch.getName())){
            saves.get(ch.getName()).update(ch.getHp(),ch.getMp(),ch.getMaxHP(),ch.getMaxMP(),ch.getLvl(),ch.getWeapon());
        }else{
            saves.put(ch.getName(),new HeroSnapShot(ch.getHp(),ch.getMp(),ch.getMaxHP(),ch.getMaxMP(),ch.getLvl(),ch.getWeapon()));
        }
    }
    public static void loadLastHeroSave(Hero ch){
        HeroSnapShot hsh = saves.get(ch.getName());
        if(hsh != null){
            ch.setHp(hsh.getHp());
            ch.setMp(hsh.getMp());
            ch.setMaxMP(hsh.getMaxMP());
            ch.setMaxHP(hsh.getMaxHP());
            ch.setLvl(hsh.getLvl());
            ch.setWeapon(hsh.getWeapon());
        }
    }
    public static String attack(ICharacter ch1, ICharacter ch2){
        if(ch1.isDead()){
            return String.format( ch1.getName() + " is dead.");
        }
        if (ch2.isDead()){
            return String.format( ch2.getName() + " is dead.");
        }else{
            ch2.getDamage(ch1.damageValue());
            return String.format(ch1.getName() + " attack " + ch2.getName() + ": -" + ch1.damageValue() + "HP");
        }
    }

}
