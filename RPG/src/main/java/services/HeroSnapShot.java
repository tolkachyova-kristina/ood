package services;

import characters.components.Weapon;

public class HeroSnapShot {
    private int hp;
    private int mp;
    private int maxHP;
    private int maxMP;
    private int lvl;
    private Weapon weapon;

    public HeroSnapShot(int hp, int mp, int maxHP, int maxMP, int lvl, Weapon weapon) {
        this.hp = hp;
        this.mp = mp;
        this.maxHP = maxHP;
        this.maxMP = maxMP;
        this.lvl = lvl;
        this.weapon = weapon;
    }
    public void update(int hp, int mp, int maxHP, int maxMP, int lvl, Weapon weapon) {
        this.hp = hp;
        this.mp = mp;
        this.maxHP = maxHP;
        this.maxMP = maxMP;
        this.lvl = lvl;
        this.weapon = weapon;
    }

    public int getHp() {
        return hp;
    }

    public int getMp() {
        return mp;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getMaxMP() {
        return maxMP;
    }

    public int getLvl() {
        return lvl;
    }

    public Weapon getWeapon() {
        return weapon;
    }
}
