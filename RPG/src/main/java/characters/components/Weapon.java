package characters.components;

public class Weapon {
    private WeaponType weaponType;
    private int damage;

    public Weapon(WeaponType weaponType, int attack) {
        this.weaponType = weaponType;
        this.damage = attack;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
