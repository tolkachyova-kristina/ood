package characters.interfaces;

public interface ICharacter {
    String status();
    int damageValue();
    void getDamage(int value);
    boolean isDead();
    String getName();
}
