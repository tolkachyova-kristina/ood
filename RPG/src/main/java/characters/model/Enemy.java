package characters.model;

import characters.components.Weapon;
import characters.interfaces.ICharacter;

public class Enemy extends CharacterBase implements ICharacter {

    private Weapon weapon;

    public Enemy(int  hp, int mp) {
        super(hp, mp);
    }

    public Enemy() {
    }
    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }


    public String status() {
        return String.format("[Enemy status: hp: " + getHp() + "/" + getMaxHP() + "; mp: " + getMp() + "/" + getMaxMP() + " ]");
    }

    public int damageValue() {
        return weapon.getDamage();
    }

    public void getDamage(int value) {
        setHp(getHp() - value);
    }

    public boolean isDead() {
        return getHp()<=0;
    }
}
