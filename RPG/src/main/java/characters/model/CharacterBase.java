package characters.model;

public class CharacterBase {
    private String name;
    private int lvl;
    private int hp;
    private int mp;
    private int maxHP;
    private int maxMP;

    public CharacterBase(String name, int hp, int mp, int lvl) {
        this.name = name;
        this.hp = hp;
        this.mp = mp;
        maxHP = hp;
        maxMP = mp;
        this.lvl = lvl;
    }

    public CharacterBase(int hp, int mp) {
       this("None",hp,mp,0);
    }

    public CharacterBase() {
        this(0,0);
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getMaxMP() {
        return maxMP;
    }

    public void setMaxMP(int maxMP) {
        this.maxMP = maxMP;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }
}
