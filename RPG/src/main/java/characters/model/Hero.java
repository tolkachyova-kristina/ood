package characters.model;

import characters.components.CharacterClass;
import characters.components.Weapon;
import characters.interfaces.ICharacter;

public class Hero extends CharacterBase implements ICharacter {
    private Weapon weapon;
    private CharacterClass myCharacterClass;

    public Hero(String name, int hp, int mp) {
        super(name, hp, mp,0);
    }

    public Hero(int hp, int mp) {
        super(hp, mp);
    }

    public Hero() {
    }

    public Weapon getWeapon() {
        return weapon;
    }
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public CharacterClass getMyClass() {
        return myCharacterClass;
    }

    public void setMyClass(CharacterClass myCharacterClass) {
        this.myCharacterClass = myCharacterClass;
    }

    public String status() {
        return String.format("[name: " + getName() + "; class: " + myCharacterClass.toString() + "; hp: " + getHp()
                + "/" + getMaxHP() + "; mp: " + getMp() + "/" + getMaxMP() + " ]");
    }

    public int damageValue() {
        return weapon.getDamage();
    }

    public void getDamage(int value) {
        setHp(getHp() - value);
    }

    public boolean isDead() {
        return getHp()<=0;
    }
}
