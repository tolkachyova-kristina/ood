package builders.interfaces;

import characters.components.CharacterClass;
import characters.components.Weapon;

public interface ICharactersBuilder {
    void setParameters(int hp, int mp);
    void setWeapon(Weapon weapon);
    void setClass(CharacterClass myCharacterClass);
}
