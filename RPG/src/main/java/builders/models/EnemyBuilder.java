package builders.models;

import builders.interfaces.ICharactersBuilder;
import characters.components.CharacterClass;
import characters.components.Weapon;
import characters.model.Enemy;

public class EnemyBuilder implements ICharactersBuilder {
    private Enemy enemy;

    public EnemyBuilder() {
        enemy = new Enemy();
    }

    public void setParameters(int hp, int mp) {
        enemy.setHp(hp);
        enemy.setMp(mp);
        enemy.setMaxHP(hp);
        enemy.setMaxMP(mp);
    }


    public void setWeapon(Weapon weapon) {
        enemy.setWeapon(weapon);
    }

    public void setClass(CharacterClass myCharacterClass) {

    }

    public Enemy getResult() {
        return enemy;
    }

}
