package builders.models;

import builders.interfaces.ICharactersBuilder;
import characters.components.CharacterClass;
import characters.components.Weapon;
import characters.model.Hero;

public class HeroBuilder implements ICharactersBuilder {
    private Hero hero;

    public HeroBuilder() {
        hero = new Hero();
    }

    public void setParameters(int hp, int mp) {
        hero.setHp(hp);
        hero.setMp(mp);
        hero.setMaxHP(hp);
        hero.setMaxMP(mp);
    }

    public void setWeapon(Weapon weapon) {
        hero.setWeapon(weapon);
    }

    public void setClass(CharacterClass myCharacterClass) {
        hero.setMyClass(myCharacterClass);
    }
    public Hero getResult(){
        return hero;
    }
}
