package builders.models;

import builders.interfaces.ICharactersBuilder;
import characters.components.CharacterClass;
import characters.components.Weapon;
import characters.components.WeaponType;

public class СharacterMaker {
    public СharacterMaker() {
    }

    public void createEnemy(ICharactersBuilder builder){
        builder.setParameters(100,50);
        builder.setWeapon(new Weapon(WeaponType.CUDGEL,15));
    }
    public void createSwordHero(ICharactersBuilder builder){
        builder.setParameters(200,100);
        builder.setWeapon(new Weapon(WeaponType.SWORD,20));
        builder.setClass(CharacterClass.SWORD);
    }
    public void createBowHero(ICharactersBuilder builder){
        builder.setParameters(200,100);
        builder.setWeapon(new Weapon(WeaponType.BOW,20));
        builder.setClass(CharacterClass.BOW);
    }
    public void createCatalystHero(ICharactersBuilder builder){
        builder.setParameters(200,100);
        builder.setWeapon(new Weapon(WeaponType.CATALYST,20));
        builder.setClass(CharacterClass.CATALYST);
    }
}
