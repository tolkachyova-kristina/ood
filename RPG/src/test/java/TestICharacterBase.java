import characters.components.CharacterClass;
import characters.model.Enemy;
import characters.model.Hero;
import org.junit.Test;
import services.CharacterServiсe;

import static org.junit.Assert.assertEquals;

public class TestICharacterBase {

    @Test
    public void testFindInteger() {
        CharacterServiсe.getInstance();
        Hero hero = (Hero) CharacterServiсe.madeCharacter(CharacterClass.SWORD);
        hero.setName("Hero");
        String heroStatus = hero.status();
        CharacterServiсe.saveHeroStatus(hero);
        Enemy enemy = (Enemy) CharacterServiсe.madeCharacter(CharacterClass.MONSTER);
        enemy.setName("Hilichurp");
        while(!hero.isDead()){
            System.out.println(CharacterServiсe.attack(enemy,hero));
        }
        System.out.println(hero.status());
        CharacterServiсe.loadLastHeroSave(hero);
        System.out.println(hero.status());
        assertEquals(heroStatus, hero.status());
    }
}
